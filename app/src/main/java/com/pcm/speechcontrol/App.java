package com.pcm.speechcontrol;

import android.app.Application;

import com.pcm.speechcontrol.speech.Logger;
import com.pcm.speechcontrol.speech.Speech;

/**
 * @author Maharaja
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Speech.init(this, getPackageName());
        Logger.setLogLevel(Logger.LogLevel.DEBUG);
    }
}
