package com.pcm.speechcontrol.speech;

/**
 * @author Maharaja
 */

public class GoogleVoiceTypingDisabledException extends Exception {
    public GoogleVoiceTypingDisabledException() {
        super("Google voice typing must be enabled");
    }
}
