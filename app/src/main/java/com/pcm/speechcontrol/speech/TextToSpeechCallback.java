package com.pcm.speechcontrol.speech;

/**
 * Contains the methods which are called to notify text to speech progress status.
 *
 * @author Maharaja
 */
public interface TextToSpeechCallback {
    void onStart();
    void onCompleted();
    void onError();
}
