package com.pcm.speechcontrol.speech;

/**
 * @author Maharaja
 */
public class SpeechRecognitionNotAvailable extends Exception {
    public SpeechRecognitionNotAvailable() {
        super("Speech recognition not available");
    }
}
