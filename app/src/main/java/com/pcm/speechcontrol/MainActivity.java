package com.pcm.speechcontrol;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pcm.speechcontrol.mqtt.MqttMessageService;
import com.pcm.speechcontrol.mqtt.PahoMqttClient;
import com.pcm.speechcontrol.speech.GoogleVoiceTypingDisabledException;
import com.pcm.speechcontrol.speech.Speech;
import com.pcm.speechcontrol.speech.SpeechDelegate;
import com.pcm.speechcontrol.speech.SpeechRecognitionNotAvailable;
import com.pcm.speechcontrol.speech.SpeechUtil;
import com.pcm.speechcontrol.speech.TextToSpeechCallback;
import com.pcm.speechcontrol.speech.ui.SpeechProgressView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @author Maharaja
 */
public class MainActivity extends AppCompatActivity implements SpeechDelegate {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int PERMISSIONS_REQUEST_CODE = 100;
    private ImageButton button;
    private Button speak;
    private TextView text;
    private EditText textToSpeech;
    private SpeechProgressView progress;
    private LinearLayout linearLayout;

    private MqttAndroidClient client;
    private PahoMqttClient pahoMqttClient;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode

            linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

            ImageView view = (ImageView) findViewById(R.id.config);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showaddDialog();
                }
            });

            button = (ImageButton) findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onButtonClick();
                }
            });

            speak = (Button) findViewById(R.id.speak);
            speak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSpeakClick();
                }
            });

            text = (TextView) findViewById(R.id.text);
            textToSpeech = (EditText) findViewById(R.id.textToSpeech);
            progress = (SpeechProgressView) findViewById(R.id.progress);

            int[] colors = {
                    ContextCompat.getColor(this, android.R.color.holo_green_dark),
                    ContextCompat.getColor(this, android.R.color.darker_gray),
                    ContextCompat.getColor(this, android.R.color.black),
                    ContextCompat.getColor(this, android.R.color.holo_orange_dark),
                    ContextCompat.getColor(this, android.R.color.holo_red_dark)
            };

            progress.setColors(colors);

            //mqtt
            pahoMqttClient = new PahoMqttClient();
            client = pahoMqttClient.getMqttClient(getApplicationContext(),
                    "tcp://" + pref.getString("serverport",
                            "m13.cloudmqtt.com:16105"), "speech",
                    pref.getString("usr", "qnfcmgvm"), pref.getString("pwd", "_L5_GG0sesbX")
                    , pref.getString("topic", "speech"));
            Intent intent = new Intent(this, MqttMessageService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showaddDialog() {
        try {
            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.config, null);
            dialogBuilder.setView(dialogView);

            final EditText serverport = (EditText) dialogView.findViewById(R.id.server);
            final EditText usr = (EditText) dialogView.findViewById(R.id.username);
            final EditText pwd = (EditText) dialogView.findViewById(R.id.password);
            final EditText top = (EditText) dialogView.findViewById(R.id.topic);
            serverport.setText(pref.getString("serverport", ""));
            usr.setText(pref.getString("usr", ""));
            pwd.setText(pref.getString("pwd", ""));
            pwd.setText(pref.getString("topic", ""));
            dialogBuilder.setTitle("Config");
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //do something with edt.getText().toString();
                    if (TextUtils.isEmpty(serverport.getText().toString())) {
                        serverport.setError("Please enter path");
                        serverport.requestFocus();
                        return;
                    } else {
                        serverport.setError(null);
                        pref.edit().putString("serverport", serverport.getText().toString()).commit();
                    }
                    if (TextUtils.isEmpty(usr.getText().toString())) {
                        usr.setError("Please enter path");
                        usr.requestFocus();
                        return;
                    } else {
                        usr.setError(null);
                        pref.edit().putString("usr", usr.getText().toString()).commit();
                    }
                    if (TextUtils.isEmpty(pwd.getText().toString())) {
                        pwd.setError("Please enter path");
                        pwd.requestFocus();
                        return;
                    } else {
                        pwd.setError(null);
                        pref.edit().putString("pwd", pwd.getText().toString()).commit();
                    }
                    if (TextUtils.isEmpty(pwd.getText().toString())) {
                        top.setError("Please enter topic");
                        top.requestFocus();
                        return;
                    } else {
                        top.setError(null);
                        pref.edit().putString("topic", top.getText().toString()).commit();
                    }
                }
            });
            dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //pass
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            Speech.getInstance().shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showError() {
        Toast.makeText(this, "Allow external storage reading", Toast.LENGTH_SHORT).show();
    }

    private void checkPermissionsAndOpen() {
        String permission = Manifest.permission.RECORD_AUDIO;

        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                showError();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
            }
        } else {
            onRecordAudioPermissionGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsAndOpen();
                } else {
                    showError();
                }
            }
        }
    }

    private void onButtonClick() {
        try {
            if (Speech.getInstance().isListening()) {
                Speech.getInstance().stopListening();
            } else {
                checkPermissionsAndOpen();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onRecordAudioPermissionGranted() {
        button.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);

        try {
            Speech.getInstance().stopTextToSpeech();
            Speech.getInstance().startListening(progress, MainActivity.this);

        } catch (SpeechRecognitionNotAvailable exc) {
            showSpeechNotSupportedDialog();

        } catch (GoogleVoiceTypingDisabledException exc) {
            showEnableGoogleVoiceTyping();
        }
    }

    private void onSpeakClick() {
        try {
            String data = textToSpeech.getText().toString().trim();
            if (data.isEmpty()) {
                Toast.makeText(this, R.string.input_something, Toast.LENGTH_LONG).show();
                return;
            }

            sentMQTT(data.toLowerCase());

            Speech.getInstance().say(textToSpeech.getText().toString().trim(), new TextToSpeechCallback() {
                @Override
                public void onStart() {
                    //Toast.makeText(MainActivity.this, "TTS onStart", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCompleted() {
                    //Toast.makeText(MainActivity.this, "TTS onCompleted", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError() {
                    //Toast.makeText(MainActivity.this, "TTS onError", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onStartOfSpeech() {
    }

    @Override
    public void onSpeechRmsChanged(float value) {
        //Log.d(getClass().getSimpleName(), "Speech recognition rms is now " + value +  "dB");
    }

    @Override
    public void onSpeechResult(String result) {
        try {
            button.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);

            text.setText(result);
            sentMQTT(result.toLowerCase());

            if (result.isEmpty()) {
                Speech.getInstance().say(getString(R.string.repeat));

            } else {
                Speech.getInstance().say(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSpeechPartialResults(List<String> results) {
        try {
            text.setText("");
            for (String partial : results) {
                text.append(partial + " ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sentMQTT(String data) {
        try {
            if (data.contains("left")) {
                pahoMqttClient.publishMessage(client, "L", 1, "uzjwwovh/feeds/onoffl-left");
            } else if (data.contains("right")) {
                pahoMqttClient.publishMessage(client, "R", 1, "uzjwwovh/feeds/onoffr-right");
            } else if (data.contains("up") || data.contains("forward")) {
                pahoMqttClient.publishMessage(client, "F", 1, "uzjwwovh/feeds/onofff-forward");
            } else if (data.contains("down") || data.contains("stop")) {
                pahoMqttClient.publishMessage(client, "S", 1, "uzjwwovh/feeds/onoffs-stop");
            }
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showSpeechNotSupportedDialog() {
        try {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            SpeechUtil.redirectUserToGoogleAppOnPlayStore(MainActivity.this);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.speech_not_available)
                    .setCancelable(false)
                    .setPositiveButton(R.string.yes, dialogClickListener)
                    .setNegativeButton(R.string.no, dialogClickListener)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEnableGoogleVoiceTyping() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_google_voice_typing)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // do nothing
                    }
                })
                .show();
    }
}
